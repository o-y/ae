/* [LGPL] Copyright 2010, 2011 Gima, Irah

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package ae.structs;

public class Vector3f implements Cloneable {
	
	private float x;
	private float y;
	private float z;
	
	public Vector3f() {
		this(-1, -1, -1);
	}
	
	public Vector3f(Vector3f source) {
		this(source.x, source.y, source.z);
	}
	
	public Vector3f(float x, float y, float z) {
		setXYZ(x, y, z);
	}
	
	public void setXYZ(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}
	
	public void moveTo(Vector3f target) {
		this.x = target.x;
		this.y = target.y;
		this.z = target.z;
	}
	
	@Override
	public Vector3f clone() {
		return new Vector3f(x, y, z);
	}
	
}
