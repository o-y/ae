/* [LGPL] Copyright 2010, 2011, 2012 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package ae.gl.texture;

import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import ae.AE;
import ae.routines.S;
import ae.sys.DirectBuffers;

public class GLTexture {
	
	private final String name;
	private final int width;
	private final int height;
	private final int glTextureID;
	private final ByteBuffer data;
	
	public GLTexture(int width, int height, String name) {
		this.name = name;
		this.width = width;
		this.height = height;
		glTextureID = GLTextureRoutines.allocateEmptyTexture(width, height);
		data = DirectBuffers.allocateNativeOrderByteBuffer(width*height*4);
		
		if (AE.isDebug()) S.debugFunc("new %s", toString());
	}
	
	@Override
	protected void finalize() throws Throwable {
		dispose();
	}

	@Override
	public String toString() {
		return S.sprintf("[Name:%s, ID:%d, Width:%d, Height:%d, Data:%s]",
				name, glTextureID, width, height, data);
	}
	
	/**
	 * Free texture ID and allocated native memory if necessary.
	 */
	public void dispose() {
		DirectBuffers.freeNativeBufferMemory(data);
		GLTextureRoutines.deallocateGLTextureID(glTextureID);
	}
	
	public String getName() {
		return name;
	}
	
	public Integer getTextureID() {
		return glTextureID;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public ByteBuffer getData() {
		return data;
	}
	
	/**
	 * Update gl texture with the buffer {@link #getData()}'s data
	 * @param data - Texture data in BGRA format.
	 */
	public void updateTextureData() {
		data.rewind(); // rewind buffer, discard mark

		int prevTextureID = GLTextureRoutines.bindTexture(glTextureID);
		GL11.glTexSubImage2D(
				GL11.GL_TEXTURE_2D,
				0,
				0, 0,
				width, height,
				GL12.GL_BGRA,
				GL11.GL_UNSIGNED_BYTE,
				data
				);
		GLTextureRoutines.bindTexture(prevTextureID);
	}
}
