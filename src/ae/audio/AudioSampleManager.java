package ae.audio;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import ae.AE;
import ae.routines.S;

public class AudioSampleManager {
	 
	private HashMap<String, OGGLoader> audioMap;
	private static AudioSampleManager INSTANCE;
//	private URL audioPath;

	public AudioSampleManager() {
		audioMap = new HashMap<String, OGGLoader>();
	}
	
	public void initialize() {
		INSTANCE = this;
	}
	
	public static AudioSampleManager getInstance() {
		return INSTANCE;
	}
	
	public void loadAudio(String filename, String identifier){
		if (audioMap.containsKey(identifier)) return;
		
		try {
			audioMap.put(identifier, new OGGLoader(new URL("file:"+filename)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void playAudio(String identifier){
		
		if(!audioMap.containsKey(identifier)) {
			if (AE.isDebug()) S.debugFunc("No audio loaded for identifier '%s'", identifier);
			return;
		}
		
		audioMap.get(identifier).play();
		
	}
	
//	public void setAudioPath(URL resourcePath) {
//		this.audioPath = resourcePath;
//	}

	public void requestShutdown() {
		for(OGGLoader handler : audioMap.values()){
			handler.close();
		}
	}

}
