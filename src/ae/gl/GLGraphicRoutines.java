/* [LGPL] Copyright 2010, 2011 Irah

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package ae.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

public class GLGraphicRoutines {

	//Initializing and setting up Projection view
	public static void initPerspective(float angle) {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glPixelZoom( 1.0f, 1.0f );
		GL11.glViewport(0, 0, GLValues.screenWidth, GLValues.screenHeight);
		GLU.gluPerspective(angle, GLValues.glRatio, 0.001f, GLValues.glDepth);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}
	
	//Initializing and setting up Ortographic view
	public static void initOrtho(){
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glPixelZoom( 1.0f, 1.0f );
		GL11.glViewport(0, 0, GLValues.screenWidth, GLValues.screenHeight);
		GL11.glOrtho( 0, GLValues.glWidth, GLValues.glHeight, 0, GLValues.glDepth, -GLValues.glDepth);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}

	//Routine for drawing background
	public static void draw2DRect(float x0, float y0, float x1, float y1, float z){
		
		GL11.glBegin( GL11.GL_QUADS );
			GL11.glTexCoord2d(0,1); GL11.glVertex3d(x0, y1, z);
			GL11.glTexCoord2d(1,1); GL11.glVertex3d(x1, y1, z);
			GL11.glTexCoord2d(1,0); GL11.glVertex3d(x1, y0, z);
			GL11.glTexCoord2d(0,0); GL11.glVertex3d(x0, y0, z);
		GL11.glEnd();
		
	}
	
	//Routine for drawing background
	public static void drawBackgroundPlane(){
		initOrtho();
		draw2DRect(0, 0, GLValues.glWidth, GLValues.glHeight, GLValues.glDepth);
	}

	public static void drawRepeatedBackgroundPlane(float rx, float ry) {
		
		initOrtho();

		GL11.glBegin( GL11.GL_QUADS );
			GL11.glTexCoord2d(0,ry); GL11.glVertex3d(0,GLValues.glHeight, GLValues.glDepth);
			GL11.glTexCoord2d(rx,ry); GL11.glVertex3d(GLValues.glWidth,GLValues.glHeight,GLValues.glDepth);
			GL11.glTexCoord2d(rx,0); GL11.glVertex3d(GLValues.glWidth,0,GLValues.glDepth);
			GL11.glTexCoord2d(0,0); GL11.glVertex3d(0,0,GLValues.glDepth);
		GL11.glEnd();
		
	}

	public static void drawCircle(float x, float y, float z, float d) {
		
		GL11.glBegin( GL11.GL_TRIANGLE_FAN );
		
		for(float a=0; a < 2*Math.PI; a += (2.0f*Math.PI)/(8*d)){
			GL11.glTexCoord2d((Math.sin(a)+1.0f)/2.0f, (Math.cos(a)+1.0f)/2.0f);
			GL11.glVertex3d(x + Math.sin(a)*d, y + Math.cos(a)*d, z);
		}
		
		GL11.glEnd();
		
	}
	
}
