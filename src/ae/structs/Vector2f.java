/* [LGPL] Copyright 2010, 2011 Gima, Irah

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package ae.structs;

public class Vector2f implements Cloneable {
	
	private float x;
	private float y;
	
	public Vector2f() {
		this(-1, -1);
	}
	
	public Vector2f(Vector2f source) {
		this(source.x, source.y);
	}
	
	public Vector2f(float x, float y) {
		setXY(x, y);
	}
	
	public void setXY(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public void moveTo(Vector2f target) {
		this.x = target.x;
		this.y = target.y;
	}
	
	@Override
	public Vector2f clone() {
		return new Vector2f(x, y);
	}
	
}
